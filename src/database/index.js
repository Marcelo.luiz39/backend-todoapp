const { Sequelize } = require('sequelize')

const User = require('../app/models/User')
const Task = require('../app/models/Task')

const configDatabase = require('../config/database')

require('dotenv/config')

const models = [User, Task]

class Database {
  constructor() {
    this.init()
  }

  init() {
    this.connection = new Sequelize(configDatabase)
    models
      .map((model) => model.init(this.connection))
      .map(
        (model) => model.associate && model.associate(this.connection.models)
      )
  }
}

module.exports = new Database()
